!############################################################
!############################################################
!############################################################
!############################################################
subroutine boundana(x,u,dx,ibound,ncell)
  use amr_parameters, ONLY: dp,ndim,nvector,boxlen
  use hydro_parameters, ONLY: nvar,boundary_var,gamma
  use disk_parameters, ONLY: rho0, rho0_frac_floor, T_halo, halo_rotation, halo_sound_speed
  use poisson_parameters, ONLY: gravity_params
  implicit none
  integer ::ibound                        ! Index of boundary region
  integer ::ncell                         ! Number of active cells
  real(dp)::dx                            ! Cell size
  real(dp),dimension(1:nvector,1:nvar)::u ! Conservative variables
  real(dp),dimension(1:nvector,1:ndim)::x ! Cell center position.
  !================================================================
  ! This routine generates boundary conditions for RAMSES.
  ! Positions are in user units:
  ! x(i,1:3) are in [0,boxlen]**ndim.
  ! U is the conservative variable vector. Conventions are here:
  ! U(i,1): d, U(i,2:ndim+1): d.u,d.v,d.w and U(i,ndim+2): E.
  ! U is in user units.
  ! ibound is the index of the boundary region defined in the namelist.
  !================================================================
  integer::ivar,i

  ! Mydisk Variables
  real(dp)::gmass,emass
  real(dp)::rho                                   ! Primitive Fluid Variables
  real(dp),dimension(1:ncell)::p
  real(dp),dimension(1:ncell)::vk2,vk,vx,vy,vz    ! Velocities
  real(dp),dimension(1:ncell)::r0,z0              ! Cylindrical Radius, Height
  real(dp),dimension(1:ncell,1:ndim)::x0          ! Box Centered Coordinates
  real(dp)::R_specific                            ! Sound Speed Stuff
  real(dp),dimension(1:ncell)::cs2_Halo

  do ivar=1,nvar
    do i=1,ncell
      u(i,ivar)=boundary_var(ibound,ivar)
    end do
  end do
  
  ! Add here, if you wish, some user-defined boudary conditions
  ! ........

  ! Set Density Floor
  rho = rho0 * rho0_frac_floor

  ! Read Gravity Params
  gmass = gravity_params(1)
  emass = gravity_params(2)

  ! Calculate Coordinates If Needed
  if ( halo_rotation == 'kepler' .or. &
     & halo_sound_speed == 'disk' .or. &
     & halo_sound_speed == 'kepler' .or. &
     & halo_sound_speed == 'kepler_mid' ) then
    ! Cylindrical Radius, Height
    x0(1:ncell,1:ndim) = x(1:ncell,1:ndim) - 0.5 * boxlen
#if NDIM==1
    r0(1:ncell) = x0(1:ncell,1)
#endif
#if NDIM>1
    r0(1:ncell) = sqrt(x0(1:ncell,1)**2.0 + x0(1:ncell,2)**2.0)
#endif
    z0(1:ncell) = 0.0d0
#if NDIM==3
    z0(1:ncell) = x0(1:ncell,3)
#endif
  end if

  ! Select Appropriate Rotation Profile
  if ( halo_rotation == 'kepler' ) then

    ! Keplerian Speed
    vk2(1:ncell) = gmass * r0(1:ncell)**2. / &
                 & (r0(1:ncell)**2. + z0(1:ncell)**2. + emass**2.)**(3./2.)
    vk(1:ncell) = sqrt(vk2(1:ncell))

    ! Cartesian Velocities
    vx(1:ncell) = - vk(1:ncell) * x0(1:ncell,2) / r0(1:ncell)
    vy(1:ncell) = + vk(1:ncell) * x0(1:ncell,1) / r0(1:ncell)
    vz(1:ncell) = 0.0d0
  else if ( halo_rotation == 'none' ) then
    vx(1:ncell) = 0.0d0
    vy(1:ncell) = 0.0d0
    vz(1:ncell) = 0.0d0
  else
    write(*,*)'Invalid Halo Rotation Profile. Terminating.'
    call clean_stop
  end if

  if ( halo_sound_speed == 'disk' ) then
    do i=1,ncell
      call compute_cs2(r0(i), cs2_Halo(i))
    end do
  else if ( halo_sound_speed == 'floor' ) then
    ! Temperature Floor in Halo
    ! T_halo is set in Namelist; cf. disk.f90
    R_specific = 4.68577596774d-06    ! AU2 TU-2 K-2 (=kB/m) (Molecular Hydrogen)
    cs2_Halo(1:ncell) = gamma * R_specific * T_halo
  else if ( halo_sound_speed == 'kepler' ) then
    cs2_Halo(1:ncell) = vk2(1:ncell)
  else if ( halo_sound_speed == 'kepler_mid' ) then
    cs2_Halo(1:ncell) = gmass * r0(1:ncell)**2. / &
                      & ( r0(1:ncell)**2. + emass**2. )**(3./2.)
  else
    write(*,*)'Invalid Halo Sound Speed Requested.'
    call clean_stop
  end if

  ! Set Pressure
  p(1:ncell) = cs2_Halo(1:ncell) * rho / gamma

  ! -------------------------------------------
  ! COPIED OVER FROM CONDINIT.F90 WITH CHANGES:
  ! nn => ncell
  ! q(:,1) => rho(:)
  ! q(:,2) => vx(:)
  ! q(:,3) => vy(:)
  ! q(:,4) => vz(:)
  ! q(:,5) => p(:)
  ! -------------------------------------------

  ! Convert primitive to conservative variables
  ! density -> density
  u(1:ncell,1)=rho
  ! velocity -> momentum
  u(1:ncell,2)=rho*vx(1:ncell)
#if NDIM>1
  u(1:ncell,3)=rho*vy(1:ncell)
#endif
#if NDIM>2
  u(1:ncell,4)=rho*vz(1:ncell)
#endif
  ! kinetic energy
  u(1:ncell,ndim+2)=0.0d0
  ! u(1:nn,ndim+2)=u(1:nn,ndim+2)+0.5*q(1:nn,1)*q(1:nn,2)**2
  u(1:ncell,ndim+2)=u(1:ncell,ndim+2)+0.5*rho*vx(1:ncell)**2
#if NDIM>1
  ! u(1:nn,ndim+2)=u(1:nn,ndim+2)+0.5*q(1:nn,1)*q(1:nn,3)**2
  u(1:ncell,ndim+2)=u(1:ncell,ndim+2)+0.5*rho*vy(1:ncell)**2
#endif
#if NDIM>2
  ! u(1:nn,ndim+2)=u(1:nn,ndim+2)+0.5*q(1:nn,1)*q(1:nn,4)**2
  u(1:ncell,ndim+2)=u(1:ncell,ndim+2)+0.5*rho*vz(1:ncell)**2
#endif
  ! pressure -> total fluid energy
  u(1:ncell,ndim+2)=u(1:ncell,ndim+2)+p(1:ncell)/(gamma-1.0d0)
  ! passive scalars
  ! do ivar=ndim+3,nvar
  !     u(1:nn,ivar)=q(1:nn,1)*q(1:nn,ivar)
  ! end do

end subroutine boundana
