##[ramses](https://bitbucket.org/rteyssie/ramses) is an open source code to   model   astrophysical    systems,   featuring   self-gravitating, magnetised, compressible,  radiative fluid flows.  It is based  on the Adaptive Mesh  Refinement (AMR)  technique on a  fully-threaded graded octree. [ramses](https://bitbucket.org/rteyssie/ramses) is written in  Fortran 90 and is making intensive use of the Message Passing Interface (MPI) library.


