subroutine cooling_fine(ilevel)
  use amr_commons
  use hydro_commons
  use cooling_module
#ifdef RT
  use rt_parameters, only: rt_UV_hom,rt_isDiffuseUVsrc
  use rt_cooling_module, only: update_UVrates
  use UV_module
#endif
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
#endif
  integer::ilevel
  !-------------------------------------------------------------------
  ! Compute cooling for fine levels
  !-------------------------------------------------------------------
  integer::ncache,i,igrid,ngrid,info
  integer,dimension(1:nvector),save::ind_grid

  if(numbtot(1,ilevel)==0)return
  if(verbose)write(*,111)ilevel

  ! Compute sink accretion rates
  if(sink)call compute_accretion_rate(0)

  ! Operator splitting step for cooling source term
  ! by vector sweeps
  ncache=active(ilevel)%ngrid
  do igrid=1,ncache,nvector
     ngrid=MIN(nvector,ncache-igrid+1)
     do i=1,ngrid
        ind_grid(i)=active(ilevel)%igrid(igrid+i-1)
     end do
     call coolfine1(ind_grid,ngrid,ilevel)
  end do

  if((cooling.and..not.neq_chem).and.ilevel==levelmin.and.cosmo)then
     if(myid==1)write(*,*)'Computing new cooling table'
     call set_table(dble(aexp))
  endif
#ifdef RT
  if(neq_chem.and.ilevel==levelmin) then
     if(cosmo)call update_rt_c
     if(cosmo .and. rt_UV_hom)call update_UVrates
     if(cosmo .and. rt_isDiffuseUVsrc)call update_UVsrc
     if(ilevel==levelmin) call output_rt_stats
  endif
#endif

111 format('   Entering cooling_fine for level',i2)

end subroutine cooling_fine
!###########################################################
!###########################################################
!###########################################################
!###########################################################
subroutine coolfine1(ind_grid,ngrid,ilevel)
  use amr_commons
  use hydro_commons
  use disk_parameters, only: cooling_time, rho0, rho0_frac_reset, r_accrete, &
                           & reset_halo, cooling_scheme
  use cooling_module
#ifdef ATON
  use radiation_commons, ONLY: Erad
#endif
#ifdef RT
  use rt_parameters, only: nGroups, iGroups
  use rt_hydro_commons
  use rt_cooling_module, only: n_U,iNpU,iFpU,rt_solve_cooling
#endif
  implicit none
  integer::ilevel,ngrid
  integer,dimension(1:nvector)::ind_grid
  !-------------------------------------------------------------------
  !-------------------------------------------------------------------
  integer::i,ind,iskip,idim,nx_loc,ix,iy,iz
  integer,dimension(1:nvector),save::ind_cell
  real(dp),dimension(1:3)::skip_loc
  real(dp)::dt,dx,vol,scale
  real(dp)::u,v,w,d,eint,etot,ekin
  real(dp)::u_ic,v_ic,w_ic,d_ic,eint_ic,etot_ic,ekin_ic
  real(dp)::u_bc,v_bc,w_bc,d_bc,eint_bc,etot_bc,ekin_bc

  ! Cooling Stuff
  real(dp)::Lambda,dtrad,Rspec,sb,eint_irr,Tirr,cs2irr,kappa,dtau,tau,ftau,Tcur
  real(dp),dimension(1:nvector)::r0

  ! Cell Position Stuff
  real(dp),dimension(1:nvector,1:ndim),save::xx,x0
  real(dp)::dd0
  real(dp),dimension(1:twotondim,1:3)::xc

  ! Conservative State Vector from ICs
  real(dp),dimension(1:nvector,1:nvar),save::uu

  ! Conservative State Vector from BCs
  real(dp),dimension(1:nvector,1:nvar),save::uuBound

  ! ***********************************************************************
  ! >>> Cell/grid coordinate calculations from hydro/init_flow_fine.f90 <<<
  ! ***********************************************************************

  ! Mesh spacing at that level
  nx_loc=icoarse_max-icoarse_min+1
  scale=boxlen/dble(nx_loc)
  dx=0.5D0**ilevel*scale
  vol=dx**ndim

  ! Set position of cell centers relative to grid center
  do ind=1,twotondim
    iz=(ind-1)/4
    iy=(ind-1-4*iz)/2
    ix=(ind-1-2*iy-4*iz)
    if(ndim>0)xc(ind,1)=(dble(ix)-0.5D0)*dx/scale
    if(ndim>1)xc(ind,2)=(dble(iy)-0.5D0)*dx/scale
    if(ndim>2)xc(ind,3)=(dble(iz)-0.5D0)*dx/scale
  end do

  ! Local Constants
  skip_loc=(/0.0d0,0.0d0,0.0d0/)
  if(ndim>0)skip_loc(1)=dble(icoarse_min)
  if(ndim>1)skip_loc(2)=dble(jcoarse_min)
  if(ndim>2)skip_loc(3)=dble(kcoarse_min)

  ! Loop over cells
  do ind=1,twotondim
     iskip=ncoarse+(ind-1)*ngridmax
     do i=1,ngrid
        ind_cell(i)=iskip+ind_grid(i)
     end do

     ! Compute Locations of Cells
     do idim=1,ndim
        do i=1,ngrid
           xx(i,idim)=xg(ind_grid(i),idim)+xc(ind,idim)
        end do
     end do

     ! Rescale
     do idim=1,ndim
        do i=1,ngrid
           xx(i,idim)=(xx(i,idim)-skip_loc(idim))*scale
        end do
     end do

     ! Shift Origin
     do idim=1,ndim
        do i=1,ngrid
           x0(i,idim)=xx(i,idim)-0.5*boxlen
        end do
     end do

     ! Cylindrical Coordinates
     do i=1,ngrid
        r0(i) = sqrt(x0(i,1)**2. + x0(i,2)**2.)
     end do
     
     ! Call Initian Conditions Routine
     call condinit(xx,uu,dx,ngrid)

     ! Call Boundary Conditions Routine
     call boundana(xx,uuBound,dx,1,ngrid)

     ! Loop Over Cells
     do i=1,ngrid
        ! Proceed If Leaf
        if (son(ind_cell(i)) == 0) then
           ! Compute Internal Energy from ICs (Floor!)
           u_ic = 0.0d0; v_ic = 0.0d0; w_ic = 0.0d0
           d_ic = uu(i,1)
           if(ndim>0) u_ic = uu(i,2) / d_ic
           if(ndim>1) v_ic = uu(i,3) / d_ic
           if(ndim>2) w_ic = uu(i,4) / d_ic
           etot_ic = uu(i,ndim+2) / d_ic
           ekin_ic = 0.5d0 * (u_ic**2.0 + v_ic**2.0 + w_ic**2.0)
           eint_ic = etot_ic - ekin_ic

           ! Compute Internal Energy from BCs
           u_bc = 0.0d0; v_bc = 0.0d0; w_bc = 0.0d0
           d_bc = uuBound(i,1)
           if(ndim>0) u_bc = uuBound(i,2) / d_bc
           if(ndim>1) v_bc = uuBound(i,3) / d_bc
           if(ndim>2) w_bc = uuBound(i,4) / d_bc
           etot_bc = uuBound(i,ndim+2) / d_bc
           ekin_bc = 0.5d0 * (u_bc**2.0 + v_bc**2.0 + w_bc**2.0)
           eint_bc = etot_bc - ekin_bc

           ! Compute Internal Energy
           u = 0.0d0; v = 0.0d0; w = 0.0d0
           d = uold(ind_cell(i),1)
           if(ndim>0) u = uold(ind_cell(i),2) / d
           if(ndim>1) v = uold(ind_cell(i),3) / d
           if(ndim>2) w = uold(ind_cell(i),4) / d
           etot = uold(ind_cell(i),ndim+2) / d
           ekin = 0.5d0 * (u**2.0 + v**2.0 + w**2.0)
           eint = etot - ekin

           if (isothermal) then
              ! Reset Temperature to IC Profile
              eint = eint_ic
           else
              if ( cooling_scheme == 'b09g12' ) then

                 ! Init Constants
                 sb = 3.60995632202d-18        ! Msolar TU-3 K-4 (Stefan-Bolztmann)
                 Rspec = 4.68577596774d-06     ! AU2 TU-2 K-2 (=kB/2/amu) (H2)

                 ! Get Local Irradiation Temperature in K
                 call compute_cs2(r0(i), cs2irr)
                 Tirr = cs2irr / gamma / Rspec
                 
                 ! Get Current Temperature in K
                 Tcur = eint / Rspec * (gamma - 1.0)
                 
                 ! Compute Opacity [cm2 g-1]
                 kappa = sqrt(max(Tcur, Tirr) / 64.0d0)
                 
                 ! Convert
                 kappa = kappa / (1.49597871d13)**2.   ! au2 g-1
                 kappa = kappa * 1.9891d33             ! au2 msun-1
                 
                 ! Compute Target Cooling/Heating Rate
                 dtau = d * kappa * dx
                 ftau = dtau + 1. / dtau
                 Lambda = - 6. / dx * sb * ( Tcur**4. - Tirr**4. ) / ftau

                 ! Estimate Radiation Step
                 dtrad = d * eint / abs(Lambda)

                 ! Internal Energy @ Equilibrium
                 eint_irr = Rspec * Tirr / (gamma - 1.0)

                 ! Adjust Rate To Use Hydro Step
                 Lambda = Lambda * exp(-(dtnew(ilevel)/dtrad)**2.0) + &
                        & d * ( eint_irr - eint ) / dtnew(ilevel) * & 
                        & ( 1.0 - exp(-(dtnew(ilevel) / dtrad)**2.0) )

                 ! Cool/Heat
                 eint = eint + dtnew(ilevel) * Lambda

              else if ( cooling_scheme == 'global_rate' ) then
                 ! Remove Internal Energy, IC Profile Gives Floor
                 ! eint = eint * (1.0 - dtnew(ilevel) / cooling_time)
                 ! Analytical Solution
                 eint = eint * exp(-dtnew(ilevel) / cooling_time)
                 ! Force Floor
                 eint = max(eint, eint_ic)
              else if ( cooling_scheme == 'local_rate' ) then
                 write(*,*)'Cooling Scheme Not Implemented. Terminating.'
                 call clean_stop
              else
                 write(*,*)'Invalid Cooling Scheme. Terminating.'
                 call clean_stop
              end if
           end if

           ! Reset Outside Disk
#if NDIM==1
           dd0 = x0(i,1)
#endif
#if NDIM==2
           dd0 = sqrt(x0(i,1)**2.0 + x0(i,2)**2.0)
#endif
#if NDIM==3
           dd0 = sqrt(x0(i,1)**2.0 + x0(i,2)**2.0 + x0(i,3)**2.0)
#endif
           if ( (reset_halo .and. d <= rho0 * rho0_frac_reset) &
              & .or. dd0 <= r_accrete ) then
              eint = eint_bc
           end if

           ! Compute Total Energy
           etot = eint + ekin
           uold(ind_cell(i),ndim+2) = etot * d
        end if
     end do

  end do
  ! End loop over cells

end subroutine coolfine1
