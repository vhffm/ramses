!================================================================
!================================================================
!================================================================
!================================================================
subroutine condinit(x,u,dx,nn)
  use amr_parameters
  use hydro_parameters
  use poisson_parameters                  ! Needed for gravity_params
  use disk_parameters
  implicit none
  integer ::nn                            ! Number of cells
  real(dp)::dx                            ! Cell size
  real(dp),dimension(1:nvector,1:nvar)::u ! Conservative variables
  real(dp),dimension(1:nvector,1:ndim)::x ! Cell center position.
  !================================================================
  ! This routine generates initial conditions for RAMSES.
  ! Positions are in user units:
  ! x(i,1:3) are in [0,boxlen]**ndim.
  ! U is the conservative variable vector. Conventions are here:
  ! U(i,1): d, U(i,2:ndim+1): d.u,d.v,d.w and U(i,ndim+2): E.
  ! Q is the primitive variable vector. Conventions are here:
  ! Q(i,1): d, Q(i,2:ndim+1):u,v,w and Q(i,ndim+2): P.
  ! If nvar >= ndim+3, remaining variables are treated as passive
  ! scalars in the hydro solver.
  ! U(:,:) and Q(:,:) are in user units.
  !================================================================
  integer::ivar
  real(dp),dimension(1:nvector,1:nvar),save::q   ! Primitive variables

  ! ------------------------
  ! ----- BEGIN MYDISK -----
  ! ------------------------

  ! My Stuff
  real(dp),dimension(1:nvector,1:ndim)::x0    ! Cell center positions relative to boxlen/2
  real(dp),dimension(1:nvector)::r            ! r Coordinate (XY Distance)
  real(dp),dimension(1:nvector)::z            ! z Coordinate
  real(dp),dimension(1:nvector)::cs2_Mid      ! Sound Speed at Midplane (Square)
  real(dp),dimension(1:nvector)::rho          ! Density
  logical,dimension(1:nvector)::cell_in_disk  ! Cell in Disk (vs Halo)

  ! Some more variables
  real(dp)::gmass,emass             ! Gravity stuff from poisson/gravana.f90
  real(dp)::h                       ! Scale Height
  integer::ii                       ! Loop Variable

  ! Temperature Floor in Halo
  real(dp)::cs2_Halo,R_specific

  ! Some More Arrays
  real(dp),dimension(1:nvector)::cs2    ! Actual Sound Speed
  real(dp),dimension(1:nvector)::vtheta2,vtheta,vk2,vp2

  ! Some Intermediate Stuff
  real(dp),dimension(1:nvector)::dPdr

  ! -------------------
  ! Now we do our stuff
  ! -------------------

  ! Call built-in initial condition generator
  call region_condinit(x,q,dx,nn)

  ! Add here, if you wish, some user-defined initial conditions
  ! ........

  ! Set gravity variables from namelist
  gmass  = gravity_params(1)
  emass  = gravity_params(2)

  ! Shift Origin
  x0(1:nn,1:ndim) = x(1:nn,1:ndim) - 0.5 * boxlen

  ! Radial Distance from Center (X, XY)
  r(1:nn) = x0(1:nn,1)
#if NDIM>1
  r(1:nn) = sqrt( x0(1:nn,1)**2. + x0(1:nn,2)**2. )
#endif

  ! Z Distance
  z(1:nn) = 0
#if NDIM>2
  z(1:nn) = x0(1:nn,3)
#endif

  ! Sound Speed at Midplane (Constant in Z)
  do ii=1,nn
    call compute_cs2(r(ii), cs2_Mid(ii))
  end do

  ! Density Profile
  do ii=1,nn
     h = sqrt(cs2_Mid(ii) / gmass * (r(ii)**2. + emass**2.)**(3./2.))
     call compute_density(r(ii), z(ii), h, rho(ii), cell_in_disk(ii))
  end do

  ! Keplerian Velocity
  vk2(1:nn) = gmass * r(1:nn)**2. / &
            & (r(1:nn)**2. + z(1:nn)**2. + emass**2.)**(3./2.)

  ! Pressure Gradient Contribution
  ! Catch < 0
  if ( adjust_velocity_dPdr ) then
     do ii=1,nn
        call compute_dPdr(r(ii), dx, z(ii), dPdr(ii))
        vp2(ii) = r(ii) / rho(ii) * dPdr(ii)
        vp2(ii) = max(0.0d0, vp2(ii))
     end do
     vtheta2(1:nn) = vk2(1:nn) + vp2(1:nn)
  else
     vtheta2(1:nn) = vk2(1:nn)
  end if

  ! Square Root
  vtheta(1:nn) = sqrt(vtheta2(1:nn))

  ! Set XY-Velocity
  ! Z-Velocity = 0 (Hydrostatic Equilibrium)
  q(1:nn,2) = - vtheta(1:nn) / r(1:nn) * x0(1:nn,2)
#if NDIM>1
  q(1:nn,3) = + vtheta(1:nn) / r(1:nn) * x0(1:nn,1)
#endif
#if NDIM>2
  q(1:nn,4) = 0.
#endif

  ! Temperature Floor in Halo
  ! T_halo is set in Namelist; cf. disk.f90
  R_specific = 4.68577596774d-06    ! AU2 TU-2 K-2 (=kB/m) (Molecular Hydrogen)
  cs2_Halo = gamma * R_specific * T_halo

  ! Set Halo Conditions
  do ii=1,nn
    if ( .not. cell_in_disk(ii) ) then
      if ( halo_sound_speed == 'disk' ) then
        cs2(ii) = cs2_Mid(ii)
      else if ( halo_sound_speed == 'floor' ) then
        cs2(ii) = cs2_Halo
      else if ( halo_sound_speed == 'kepler' ) then
        cs2(ii) = vk2(ii)
      else if ( halo_sound_speed == 'kepler_mid' ) then
        cs2(ii) = gmass * r(ii)**2. / ( r(ii)**2. + emass**2. )**(3./2.)
      else
        write(*,*)'Invalid Halo Sound Speed Requested.'
        call clean_stop
      end if
      if ( halo_rotation == 'none' ) then
        q(ii,2) = 0.0d0
        q(ii,3) = 0.0d0
      end if
    else
      cs2(ii) = cs2_Mid(ii)
    end if
  end do

  ! Density
  q(1:nn,1) = rho(1:nn)

  ! Pressure
  q(1:nn,ndim+2) = q(1:nn,1) * cs2(1:nn) / gamma

  ! ------------------------
  ! ------ END MYDISK ------
  ! ------------------------

  ! Convert primitive to conservative variables
  ! density -> density
  u(1:nn,1)=q(1:nn,1)
  ! velocity -> momentum
  u(1:nn,2)=q(1:nn,1)*q(1:nn,2)
#if NDIM>1
  u(1:nn,3)=q(1:nn,1)*q(1:nn,3)
#endif
#if NDIM>2
  u(1:nn,4)=q(1:nn,1)*q(1:nn,4)
#endif
  ! kinetic energy
  u(1:nn,ndim+2)=0.0d0
  u(1:nn,ndim+2)=u(1:nn,ndim+2)+0.5*q(1:nn,1)*q(1:nn,2)**2
#if NDIM>1
  u(1:nn,ndim+2)=u(1:nn,ndim+2)+0.5*q(1:nn,1)*q(1:nn,3)**2
#endif
#if NDIM>2
  u(1:nn,ndim+2)=u(1:nn,ndim+2)+0.5*q(1:nn,1)*q(1:nn,4)**2
#endif
  ! pressure -> total fluid energy
  u(1:nn,ndim+2)=u(1:nn,ndim+2)+q(1:nn,ndim+2)/(gamma-1.0d0)
#if NVAR > NDIM + 2
  ! passive scalars
  do ivar=ndim+3,nvar
     u(1:nn,ivar)=q(1:nn,1)*q(1:nn,ivar)
  end do
#endif

end subroutine condinit
!================================================================
!================================================================
!================================================================
!================================================================
subroutine compute_density(r, z, h, rho, cell_in_disk)

  use amr_parameters, only  : dp
  use disk_parameters, only : r_in, r_out, rho0, rho0_frac_floor, rho0_frac_reset, r_accrete

  implicit none

  real(dp), intent(in)  :: r, z, h
  real(dp)              :: Sigma, rho_Mid, pi
  real(dp), intent(out) :: rho
  logical,  intent(out) :: cell_in_disk

  ! Pi
  pi = acos(-1.0d0)

  ! Surface Density
  Sigma = 1.0 / r * exp(-(r_in / r)**12.) * exp(-(r / r_out)**18.)

  ! Midplane Volume Density (z<<r)
  rho_Mid = Sigma / (sqrt(2*pi) * h)
  ! rho_Mid = 1. / r * exp(-(r_in / r)**2.) * exp(-(r / r_out)**2.)

  ! Volume Density (z<<r)
#if NDIM==2
  rho = rho0 * Sigma
#endif
#if NDIM==3
  rho = rho0 * rho_Mid * exp(-0.5 * z**2. / h**2.)
#endif

  if ( rho <= rho0 * rho0_frac_reset .or. sqrt(r**2. + z**2.) <= r_accrete ) then
    rho = rho0 * rho0_frac_floor
    cell_in_disk = .FALSE.
  else
    cell_in_disk = .TRUE.
  end if

end subroutine compute_density
!================================================================
!================================================================
!================================================================
!================================================================
subroutine compute_dPdr(r, dx, z, dPdr)

  use amr_parameters, only     : dp
  use hydro_parameters, only   : gamma
  use poisson_parameters, only : gravity_params

  implicit none

  real(dp), intent(in)  :: r, dx, z
  real(dp)              :: r_lo, r_hi, h_lo, h_hi
  real(dp)              :: cs2_lo, cs2_hi
  real(dp)              :: rho_lo, rho_hi
  real(dp)              :: P_lo, P_hi
  real(dp)              :: gmass,emass
  logical               :: cell_in_disk_hi, cell_in_disk_lo
  real(dp), intent(out) :: dPdr

  ! Central Mass
  gmass  = gravity_params(1)
  emass  = gravity_params(2)

  ! Radii
  r_lo = r - sqrt(2.) * dx / 1000.
  r_hi = r + sqrt(2.) * dx / 1000.

  ! Midplane Sound Speed (Z-Isothermal)
  call compute_cs2(r_lo, cs2_lo)
  call compute_cs2(r_hi, cs2_hi)

  ! Scale Heights
  h_lo = sqrt(cs2_lo / gmass * (r_lo**2. + emass**2.)**(3./2.))
  h_hi = sqrt(cs2_hi / gmass * (r_hi**2. + emass**2.)**(3./2.))

  ! Densities
  call compute_density(r_lo, z, h_lo, rho_lo, cell_in_disk_lo)
  call compute_density(r_hi, z, h_hi, rho_hi, cell_in_disk_hi)

  ! Pressures
  P_lo = cs2_lo * rho_lo / gamma
  P_hi = cs2_hi * rho_hi / gamma

  ! Pressure Derivative
  dPdr = ( P_hi - P_lo ) / ( r_hi - r_lo )

end subroutine compute_dPdr
!================================================================
!================================================================
!================================================================
!================================================================
subroutine compute_cs2(r, cs2)

  use amr_parameters, only     : dp, nlevelmax
  use disk_parameters, only    : temperature_profile, aspect, T_floor
  use poisson_parameters, only : gravity_params
  use hydro_parameters, only   : gamma
  use hydro_commons, only      : mass_lost_to_star_dt_total_last
  use amr_commons, only        : dtold, t

  implicit none

  real(dp), intent(in)  :: r
  real(dp)              :: gmass, vk2_mid, cs2_mid
  real(dp), intent(out) :: cs2

  ! Temperature Profiles
  real(dp)::pi,R_specific,sb  ! Constants
  real(dp)::Mdot              ! Mass Accretion Rate (dM/dt)
  real(dp)::R_star,T_star     ! Stellar Radius, Effective Temperature
  real(dp)::Ror               ! Rstar / Rdisk
  real(dp)::T_disk,T_disk4    ! Disk Temperature

  ! Constants in G=1 Units
  pi = acos(-1.0d0)
  R_specific = 4.68577596774d-06     ! AU2 TU-2 K-2 (=kB/m) (Molecular Hydrogen)
  T_star = 6500.0d0                  ! K (=1 Solar Effective Luminosity)
  R_star = 0.00464912632522d0        ! AU (= 1 Solar Radius)
  sb = 3.60995632202d-18             ! Msolar TU-3 K-4 (Stefan-Bolztmann)

  ! Central Mass
  gmass  = gravity_params(1)

  ! Select Temperature Profile; Compute Temperature and Sound Speed
  if ( temperature_profile == 'aspect' ) then
    ! 1 - Compute Unsoftened Midplane Keplerian Velocity
    ! 2 - Compute Sound Speed via Aspect Ratio
    vk2_mid = gmass / r
    cs2_mid = aspect**2. * vk2_mid
    T_disk  = cs2_mid / gamma / R_specific
    T_disk  = T_disk + T_floor
    cs2_mid = gamma * R_specific * T_disk
    cs2     = cs2_mid
  else if ( temperature_profile == 'equilibrium' ) then
    ! 1 - Compute Temperature Profile for Radiative Equilibrium
    !   - Cf. Armitage 2007; P11; Eq32
    ! 2 - Compute Corresponding Sound Speed
    Ror     = R_star / r
    T_disk4 = T_star**4. * 1.0d0 / pi * &
            & ( asin(Ror) - Ror * sqrt(1.0d0 - Ror**2.) )
    T_disk  = T_disk4**(1./4.) + T_floor
    cs2_mid = gamma * R_specific * T_disk
    cs2     = cs2_mid
  else if ( temperature_profile == 'active' ) then
    ! 1 - Compute Temperature Profile Corresponding to Active Disk
    !   - cf. http://ay201b.wordpress.com/2011/04/27/accretion-luminosity/
    !   -     Armitage 2007; P15; Eq71
    ! 2 - Compute Corresponding Sound Speed
    if ( mass_lost_to_star_dt_total_last > 0.0 ) then
      Mdot  = mass_lost_to_star_dt_total_last / dtold(nlevelmax)
    else
      ! For T-Tauri Phase; Mdot ~ 1.0d-7 Is Inferred
      ! Cf. Armitage 2007; Williams 2011; Armitage 2011
      Mdot  = 1.0d-8
    end if
    Ror     = R_star / r
    T_disk4 = ( 3.0 * gmass * Mdot ) / ( 8.0 * pi * sb * r**3.0 ) * &
            & ( 1.0 - sqrt( Ror ) )
    T_disk  = T_disk4**(1./4.) + T_floor
    cs2_mid = gamma * R_specific * T_disk
    cs2     = cs2_mid
  else if ( temperature_profile == 'steps' ) then
    ! Same As Active Profile
    ! Except Mdot Set Manually
    if ( (t > 12000 .and. t <= 18000) .or. &
       & (t > 24000 .and. t <= 30000) .or. &
       & (t > 36000 .and. t <= 42000) ) then
      Mdot = 1.0d-4
    else
      Mdot = 1.0d-6
    end if
    Ror     = R_star / r
    T_disk4 = ( 3.0 * gmass * Mdot ) / ( 8.0 * pi * sb * r**3.0 ) * &
            & ( 1.0 - sqrt( Ror ) )
    T_disk  = T_disk4**(1./4.) + T_floor
    cs2_mid = gamma * R_specific * T_disk
    cs2     = cs2_mid
  else if ( temperature_profile == 'flat' ) then
    T_disk  = T_floor
    cs2_mid = gamma * R_specific * T_disk
    cs2     = cs2_mid
  else
    write(*,*)'Invalid Temperature Profile. Terminating.'
    call clean_stop
  end if

end subroutine compute_cs2
