subroutine courant_fine(ilevel)
  use amr_commons
  use hydro_commons
  use poisson_commons
  use disk_parameters, only: rho0, rho0_frac_reset
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
#endif
  integer::ilevel
  !----------------------------------------------------------------------
  ! Using the Courant-Friedrich-Levy stability condition,               !
  ! this routine computes the maximum allowed time-step.                !
  !----------------------------------------------------------------------
  integer::i,ivar,idim,ind,ncache,igrid,iskip
  integer::info,nleaf,ngrid,nx_loc
  integer,dimension(1:nvector),save::ind_grid,ind_cell,ind_leaf,ind_grid_leaf

  real(dp)::dt_lev,dx,vol,scale
  real(kind=8)::mass_loc,mass_disk_loc,ekin_loc,eint_loc,dt_loc
  real(kind=8)::mass_all,mass_disk_all,ekin_all,eint_all,dt_all
  real(kind=8)::mass_halo_loc,mass_halo_all
  real(kind=8),dimension(12)::comm_buffin,comm_buffout
  real(dp),dimension(1:nvector,1:nvar),save::uu
  real(dp),dimension(1:nvector,1:ndim),save::gg

  ! Cell Position Stuff
  real(dp),dimension(1:3)::skip_loc
  real(dp),dimension(1:twotondim,1:3)::xc
  real(dp),dimension(1:nvector,1:ndim),save::xx
  integer::ix,iy,iz
  real(dp),dimension(1:nvector,1:ndim)::x0    ! Box Centered Positions

  ! Angular Momentum
  real(dp)::angular_momentum
  real(dp)::angular_momentum_loc
  real(dp)::angular_momentum_all
  real(dp)::angular_momentum_disk_loc
  real(dp)::angular_momentum_disk_all
  real(dp)::angular_momentum_halo_loc
  real(dp)::angular_momentum_halo_all

  if(numbtot(1,ilevel)==0)return
  if(verbose)write(*,111)ilevel

  mass_all=0.0d0; mass_loc=0.0d0
  ekin_all=0.0d0; ekin_loc=0.0d0
  eint_all=0.0d0; eint_loc=0.0d0
  mass_disk_all=0.0d0; mass_disk_loc=0.0d0
  mass_halo_all=0.0d0; mass_halo_loc=0.0d0
  angular_momentum_loc=0.0d0; angular_momentum_all=0.0d0
  angular_momentum_disk_loc=0.0D0; angular_momentum_disk_all=0.0D0
  angular_momentum_halo_loc=0.0D0; angular_momentum_halo_all=0.0D0
  dt_all=dtnew(ilevel); dt_loc=dt_all

  ! ***********************************************************************
  ! >>> Cell/grid coordinate calculations from hydro/init_flow_fine.f90 <<<
  ! ***********************************************************************

  ! Mesh spacing at that level
  nx_loc=icoarse_max-icoarse_min+1
  scale=boxlen/dble(nx_loc)
  dx=0.5D0**ilevel*scale
  vol=dx**ndim

  ! Set position of cell centers relative to grid center
  do ind=1,twotondim
    iz=(ind-1)/4
    iy=(ind-1-4*iz)/2
    ix=(ind-1-2*iy-4*iz)
    if(ndim>0)xc(ind,1)=(dble(ix)-0.5D0)*dx/scale
    if(ndim>1)xc(ind,2)=(dble(iy)-0.5D0)*dx/scale
    if(ndim>2)xc(ind,3)=(dble(iz)-0.5D0)*dx/scale
  end do

  ! Local Constants
  skip_loc=(/0.0d0,0.0d0,0.0d0/)
  if(ndim>0)skip_loc(1)=dble(icoarse_min)
  if(ndim>1)skip_loc(2)=dble(jcoarse_min)
  if(ndim>2)skip_loc(3)=dble(kcoarse_min)

  ! Loop over active grids by vector sweeps
  ncache=active(ilevel)%ngrid
  do igrid=1,ncache,nvector
     ngrid=MIN(nvector,ncache-igrid+1)
     do i=1,ngrid
        ind_grid(i)=active(ilevel)%igrid(igrid+i-1)
     end do
     
     ! Loop over cells
     do ind=1,twotondim        
        iskip=ncoarse+(ind-1)*ngridmax
        do i=1,ngrid
           ind_cell(i)=ind_grid(i)+iskip
        end do
        
        ! Gather leaf cells
        nleaf=0
        do i=1,ngrid
           if(son(ind_cell(i))==0)then
              nleaf=nleaf+1
              ind_leaf(nleaf)=ind_cell(i)
              ind_grid_leaf(nleaf)=ind_grid(i)
           end if
        end do

        ! Gather hydro variables
        do ivar=1,nvar
           do i=1,nleaf
              uu(i,ivar)=uold(ind_leaf(i),ivar)
           end do
        end do
        
        ! Gather gravitational acceleration
        gg=0.0d0
        if(poisson)then
           do idim=1,ndim
              do i=1,nleaf
                 gg(i,idim)=f(ind_leaf(i),idim)
              end do
           end do
        end if
        
        ! Compute total mass, disk mass
        do i=1,nleaf
           mass_loc=mass_loc+uu(i,1)*vol
           ! Density >= Outer Disk Density Contour?
           if (uu(i,1) > (rho0 * rho0_frac_reset)) then
              mass_disk_loc=mass_disk_loc+uu(i,1)*vol
           else
              mass_halo_loc=mass_halo_loc+uu(i,1)*vol
           end if
        end do

        ! Compute Locations of Leaf Cells
        do idim=1,ndim
           do i=1,nleaf
              xx(i,idim)=xg(ind_grid_leaf(i),idim)+xc(ind,idim)
           end do
        end do

        ! Rescale
        do idim=1,ndim
           do i=1,nleaf
              xx(i,idim)=(xx(i,idim)-skip_loc(idim))*scale
           end do
        end do

        ! Shift Origin
        do idim=1,ndim
           do i=1,nleaf
              x0(i,idim)=xx(i,idim)-0.5*boxlen
           end do
        end do

        ! Compute Angular Momentum for Disk & Halo
        ! uu(i,2:4) = linear momentum density
        ! x0(i,1:3) = cartesian components of distance to reference point
        do i=1,nleaf
           call compute_angular_momentum(x0(i,:), uu(i,:), angular_momentum, dx)
           angular_momentum_loc = angular_momentum_loc + &
                                & angular_momentum
           if (uu(i,1) .GE. (rho0 * rho0_frac_reset)) then
              angular_momentum_disk_loc = angular_momentum_disk_loc + &
                                        & angular_momentum
           else
              angular_momentum_halo_loc = angular_momentum_halo_loc + &
                                        & angular_momentum
           end if
        end do

        ! Compute total energy
        do i=1,nleaf
           ekin_loc=ekin_loc+uu(i,ndim+2)*vol
        end do
        
        ! Compute total internal energy
        do i=1,nleaf
           eint_loc=eint_loc+uu(i,ndim+2)*vol
        end do
        do ivar=1,ndim
           do i=1,nleaf
              eint_loc=eint_loc-0.5d0*uu(i,1+ivar)**2/uu(i,1)*vol
           end do
        end do
        
        ! Compute CFL time-step
        if(nleaf>0)then
           call cmpdt(uu,gg,dx,dt_lev,nleaf)
           dt_loc=min(dt_loc,dt_lev)
        end if
        
     end do
     ! End loop over cells
     
  end do
  ! End loop over grids

  ! Compute global quantities
#ifndef WITHOUTMPI
  comm_buffin(1)  = mass_loc
  comm_buffin(2)  = ekin_loc
  comm_buffin(3)  = eint_loc
  comm_buffin(4)  = mass_disk_loc
  comm_buffin(5)  = mass_lost_to_star_dt_leaf_loc
  comm_buffin(6)  = mass_halo_loc
  comm_buffin(7)  = mass_lost_to_halo_dt_leaf_loc
  comm_buffin(8)  = angular_momentum_loc
  comm_buffin(9)  = angular_momentum_lost_to_star_dt_leaf_loc
  comm_buffin(10) = angular_momentum_lost_to_halo_dt_leaf_loc
  comm_buffin(11) = angular_momentum_disk_loc
  comm_buffin(12) = angular_momentum_halo_loc
  call MPI_ALLREDUCE(comm_buffin,comm_buffout,12,MPI_DOUBLE_PRECISION,MPI_SUM,&
       &MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(dt_loc,dt_all,1,MPI_DOUBLE_PRECISION,MPI_MIN,&
       &MPI_COMM_WORLD,info)
  mass_all = comm_buffout(1)
  ekin_all = comm_buffout(2)
  eint_all = comm_buffout(3)
  mass_disk_all                   = comm_buffout(4)
  mass_lost_to_star_dt_leaf_level = comm_buffout(5)
  mass_halo_all                   = comm_buffout(6)
  mass_lost_to_halo_dt_leaf_level = comm_buffout(7)
  angular_momentum_all                        = comm_buffout(8)
  angular_momentum_lost_to_star_dt_leaf_level = comm_buffout(9)
  angular_momentum_lost_to_halo_dt_leaf_level = comm_buffout(10)
  angular_momentum_disk_all                   = comm_buffout(11)
  angular_momentum_halo_all                   = comm_buffout(12)
#endif
#ifdef WITHOUTMPI
  mass_all = mass_loc
  ekin_all = ekin_loc
  eint_all = eint_loc
  mass_disk_all = mass_disk_loc
  mass_halo_all = mass_halo_loc
  mass_lost_to_star_dt_leaf_level = mass_lost_to_star_dt_leaf_loc
  mass_lost_to_halo_dt_leaf_level = mass_lost_to_halo_dt_leaf_loc
  angular_momentum_all = angular_momentum_loc
  angular_momentum_lost_to_star_dt_leaf_level = angular_momentum_lost_to_star_dt_leaf_loc
  angular_momentum_lost_to_halo_dt_leaf_level = angular_momentum_lost_to_halo_dt_leaf_loc
  angular_momentum_disk_all = angular_momentum_disk_loc
  angular_momentum_halo_all = angular_momentum_halo_loc
  dt_all=dt_loc
#endif
  mass_tot = mass_tot + mass_all
  ekin_tot = ekin_tot + ekin_all
  eint_tot = eint_tot + eint_all
  mass_disk_tot = mass_disk_tot + mass_disk_all
  mass_halo_tot = mass_halo_tot + mass_halo_all
  mass_lost_to_star_dt_total = mass_lost_to_star_dt_total + mass_lost_to_star_dt_leaf_level
  mass_lost_to_halo_dt_total = mass_lost_to_halo_dt_total + mass_lost_to_halo_dt_leaf_level
  angular_momentum_total = angular_momentum_total+angular_momentum_all
  angular_momentum_lost_to_star_dt_total = angular_momentum_lost_to_star_dt_total + angular_momentum_lost_to_star_dt_leaf_level
  angular_momentum_lost_to_halo_dt_total = angular_momentum_lost_to_halo_dt_total + angular_momentum_lost_to_halo_dt_leaf_level
  angular_momentum_disk_total = angular_momentum_disk_total + angular_momentum_disk_all
  angular_momentum_halo_total = angular_momentum_halo_total + angular_momentum_halo_all
  dtnew(ilevel) = MIN(dtnew(ilevel),dt_all)

  ! Total (Accumulated Remove Mass and Angular Momentum)
  mass_lost_to_star_accumulated = mass_lost_to_star_accumulated + mass_lost_to_star_dt_total
  mass_lost_to_halo_accumulated = mass_lost_to_halo_accumulated + mass_lost_to_halo_dt_total
  angular_momentum_lost_to_star_accumulated = angular_momentum_lost_to_star_accumulated + angular_momentum_lost_to_star_dt_total
  angular_momentum_lost_to_halo_accumulated = angular_momentum_lost_to_halo_accumulated + angular_momentum_lost_to_halo_dt_total

111 format('   Entering courant_fine for level ',I2)

end subroutine courant_fine
