! -------------------------------------------------------
! Disk Parameters Modules. Contains Parameters for Disks.
! -------------------------------------------------------
module disk_parameters
  use amr_parameters, only: dp

  ! Disk Geometry
  real(dp)::r_in=0.5
  real(dp)::r_out=1.5
  real(dp)::aspect=0.05

  ! Boundary Conditions
  real(dp)::r_accrete=0.05
  real(dp)::rho0=0.01
  real(dp)::rho0_frac_reset=1.0d-3
  real(dp)::rho0_frac_floor=1.0d-9

  ! Adjust Initial Velocities ~ dP/dr?
  logical::adjust_velocity_dPdr=.true.

  ! Reset Halo Values on Isodensity Contour?
  logical::reset_halo=.TRUE.
  character(len=20)::halo_sound_speed='disk'

  ! Halo Rotation ('kepler' or 'none')
  character(len=10)::halo_rotation='kepler'

  ! Temperature Profile, Halo Temperature, Disk Temperature Floor
  character(len=20)::temperature_profile='aspect'
  real(dp)::T_halo=10.0
  real(dp)::T_floor=10.0

  ! Cooling, NB: 3000.0 ~ Orbital Time @ 60 AU
  real(dp)::cooling_time=3000.0
  character(len=20)::cooling_scheme='b09g12'

  ! Mass Infall
  real(dp),dimension(1:10)::infall_start=0.0d0
  real(dp),dimension(1:10)::infall_stop=0.0d0
  real(dp),dimension(1:10)::infall_rate=0.0d0

end module disk_parameters


! -----------------------------
! Disk_Commons Module. Whatfor?
! -----------------------------
module disk_commons
  use disk_parameters
end module disk_commons


! -------------------------------
! Read DISK_PARAMS from Namelist.
! -------------------------------
subroutine read_disk_params()
  use disk_parameters
  use cooling_module, only: twopi
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
#endif

  ! Define Namelist Block
  namelist/disk_params/ &
    & r_in, r_out, aspect, r_accrete, &
    & rho0, rho0_frac_reset, rho0_frac_floor, &
    & infall_start, infall_stop, infall_rate, &
    & reset_halo, temperature_profile, T_halo, &
    & cooling_time, halo_rotation, T_floor, halo_sound_speed, &
    & adjust_velocity_dPdr, &
    & cooling_scheme

  ! Read Disk_Params Block
  rewind(1)
  read(1, NML=disk_params, END=106)
  goto 107
106 write(*,*)' You need to set up namelist &DISK_PARAMS in parameter file'
  call clean_stop
107 continue

  ! Convert Infall Rate from Msolar/yr to Msolar/TU
  ! (Physical Units -> Code Units)
  infall_rate(1:10) = infall_rate(1:10) / twopi

end subroutine read_disk_params


! -------------------------
! Compute Angular Momentum.
! -------------------------
subroutine compute_angular_momentum(x0, uu, angular_momentum, dx)

  use amr_parameters, only: dp, ndim
  use hydro_parameters, only: nvar

  implicit none

  real(dp),dimension(1:ndim),intent(in)::x0       ! Coordinates Wrt Center
  real(dp),dimension(1:nvar),intent(in)::uu       ! Density, Momenta, Pressure

  real(dp),intent(out)::angular_momentum

  real(dp)::angular_momentum_x
  real(dp)::angular_momentum_y
  real(dp)::angular_momentum_z
  real(dp),intent(in)::dx

  ! uu(2:4) = linear momentum density
  ! x0(1:3) = cartesian components of distance to reference point

#if NDIM==3
  angular_momentum_x = x0(2) * uu(4) - x0(3) * uu(3)
  angular_momentum_y = x0(3) * uu(2) - x0(1) * uu(4)
  angular_momentum_z = x0(1) * uu(3) - x0(2) * uu(2)
#endif
#if NDIM==2
  angular_momentum_x = 0.0d0
  angular_momentum_y = 0.0d0
  angular_momentum_z = x0(1) * uu(3) - x0(2) * uu(2)
#endif
#if NDIM==1
  angular_momentum_x = 0.0d0
  angular_momentum_y = 0.0d0
  angular_momentum_z = 0.0d0
#endif

  ! Total Angular Momentum Density
  angular_momentum = sqrt(   angular_momentum_x**2. + &
                           & angular_momentum_y**2. + &
                           & angular_momentum_z**2. )

  ! Total Angular Momentum
  angular_momentum = angular_momentum * dx**ndim

end subroutine compute_angular_momentum
