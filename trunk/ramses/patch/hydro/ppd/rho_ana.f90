!#########################################################
!#########################################################
!#########################################################
!#########################################################
subroutine rho_ana(x,d,dx,ncell)
  use amr_parameters
  use hydro_parameters
  use poisson_parameters
  implicit none
  integer ::ncell                         ! Number of cells
  real(dp)::dx                            ! Cell size
  real(dp),dimension(1:nvector)       ::d ! Density
  real(dp),dimension(1:nvector,1:ndim)::x ! Cell center position.
  !================================================================
  ! This routine generates analytical Poisson source term.
  ! Positions are in user units:
  ! x(i,1:3) are in [0,boxlen]**ndim.
  ! d(i) is the density field in user units.
  !================================================================
  integer::i
  real(dp)::gmass,emass,xmass,ymass,zmass
  real(dp)::pi
  real(dp)::x0,y0,z0,dd0

  pi = acos(-1.0d0)

  ! Stellar Potential => Plummer Sphere
  if (gravity_type .EQ. -3) then
    gmass = gravity_params(1)       ! Mass
    emass = gravity_params(2)       ! Softening Length
    xmass = gravity_params(3)       ! Position
    ymass = gravity_params(4)
    zmass = gravity_params(5)

    do i=1,ncell
      x0 = x(i,1) - xmass
      y0 = x(i,2) - ymass
      z0 = x(i,3) - zmass

      dd0 = x0**2 + y0**2. + z0**2.
      
      d(i) = 3. * gmass / (4. * pi * emass**3.) * (1. + dd0 / emass**2.)**(-5./2.)
    end do
  end if
end subroutine rho_ana
