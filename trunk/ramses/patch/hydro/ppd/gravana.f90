!#########################################################
!#########################################################
!#########################################################
!#########################################################
subroutine gravana(x,f,dx,ncell)
  use amr_parameters
  use poisson_parameters  
  implicit none
  integer ::ncell                         ! Size of input arrays
  real(dp)::dx                            ! Cell size
  real(dp),dimension(1:nvector,1:ndim)::f ! Gravitational acceleration
  real(dp),dimension(1:nvector,1:ndim)::x ! Cell center position.
  !================================================================
  ! This routine computes the acceleration using analytical models.
  ! x(i,1:ndim) are cell center position in [0,boxlen] (user units).
  ! f(i,1:ndim) is the gravitational acceleration in user units.
  !================================================================
  integer::idim,i
  real(dp)::gmass,emass,xmass,ymass,zmass,rr,rx,ry,rz

  ! Constant vector
  if(gravity_type==1)then 
     do idim=1,ndim
        do i=1,ncell
           f(i,idim)=gravity_params(idim)
        end do
     end do
  end if

  ! Point mass
  if((gravity_type==2) .OR. (gravity_type == 3))then 
     gmass=gravity_params(1) ! GM
     emass=gravity_params(2) ! Softening length
     xmass=gravity_params(3) ! Point mass coordinates
     ymass=gravity_params(4)
     zmass=gravity_params(5)
     do i=1,ncell
        rx=0.0d0; ry=0.0d0; rz=0.0d0
        rx=x(i,1)-xmass
#if NDIM>1
        ry=x(i,2)-ymass
#endif
#if NDIM>2
        rz=x(i,3)-zmass
#endif
        rr=sqrt(rx**2+ry**2+rz**2+emass**2)
        f(i,1)=-gmass*rx/rr**3
#if NDIM>1
        f(i,2)=-gmass*ry/rr**3
#endif
#if NDIM>2
        f(i,3)=-gmass*rz/rr**3
#endif
     end do
  end if

end subroutine gravana
!#########################################################
!#########################################################
!#########################################################
!#########################################################
subroutine phi_ana(rr,pp,ngrid)
  use amr_commons
  use poisson_commons
  implicit none
  integer::ngrid
  real(dp),dimension(1:nvector)::rr,pp
  ! -------------------------------------------------------------------
  ! This routine set up boundary conditions for fine levels.
  ! -------------------------------------------------------------------

  integer :: i
  real(dp):: fourpi

  fourpi=4.D0*ACOS(-1.0D0)

#if NDIM==1
  do i=1,ngrid
     pp(i)=multipole(1)*fourpi/2d0*rr(i)
  end do
#endif
#if NDIM==2
  do i=1,ngrid
     pp(i)=multipole(1)*2d0*log(rr(i))
  end do
#endif
#if NDIM==3
  do i=1,ngrid
     pp(i)=-multipole(1)/rr(i)
  end do
#endif
end subroutine phi_ana
!#########################################################
!#########################################################
!#########################################################
!#########################################################
subroutine impose_bc(ilevel)
  use amr_commons
  use hydro_commons
  use disk_parameters, only: rho0, rho0_frac_reset, r_accrete, reset_halo
  implicit none
  integer::ilevel
  
  integer::igrid,ngrid,ncache,i,ind,iskip,ix,iy,iz
  integer::info,ibound,nx_loc,idim
  real(dp)::dx,dx_loc,scale,d,u,v,w,e
  real(kind=8)::rho_max_loc,rho_max_all,epot_loc,epot_all
  real(dp),dimension(1:8,1:3)::xc
  real(dp),dimension(1:3)::skip_loc

  integer ,dimension(1:nvector),save::ind_grid,ind_cell
  real(dp),dimension(1:nvector,1:ndim),save::xx
  real(dp),dimension(1:nvector,1:nvar),save::uu

  real(dp)::xx0,yy0,zz0,dd0
  real(dp),dimension(1:ndim)::x0

  real(dp)::delta_rho

  real(dp)::angular_momentum_old
  real(dp)::angular_momentum_new
  real(dp)::delta_angular_momentum
 
  if(verbose)write(*,111)ilevel

  ! Mesh size at level ilevel in coarse cell units
  dx=0.5D0**ilevel
  
  ! Rescaling factors
  nx_loc=(icoarse_max-icoarse_min+1)
  skip_loc=(/0.0d0,0.0d0,0.0d0/)
  if(ndim>0)skip_loc(1)=dble(icoarse_min)
  if(ndim>1)skip_loc(2)=dble(jcoarse_min)
  if(ndim>2)skip_loc(3)=dble(kcoarse_min)
  scale=dble(nx_loc)/boxlen
  dx_loc=dx/scale

  ! Set position of cell centers relative to grid center
  do ind=1,twotondim
     iz=(ind-1)/4
     iy=(ind-1-4*iz)/2
     ix=(ind-1-2*iy-4*iz)
     if(ndim>0)xc(ind,1)=(dble(ix)-0.5D0)*dx
     if(ndim>1)xc(ind,2)=(dble(iy)-0.5D0)*dx
     if(ndim>2)xc(ind,3)=(dble(iz)-0.5D0)*dx
  end do

  ! Reset Counters Over MPI Tasks
  mass_lost_to_star_dt_leaf_loc=0.0D0
  mass_lost_to_halo_dt_leaf_loc=0.0D0
  angular_momentum_lost_to_star_dt_leaf_loc=0.0D0
  angular_momentum_lost_to_halo_dt_leaf_loc=0.0D0
  
  ! Loop over all AMR linked lists (including physical boundaries).
  do ibound=1,nboundary+ncpu

     if(ibound==myid)then
        ncache=active(ilevel)%ngrid
     else if(ibound<=ncpu)then
        ncache=reception(ibound,ilevel)%ngrid
     else
        ncache=boundary(ibound-ncpu,ilevel)%ngrid
     end if

     ! Loop over grids by vector sweeps
     do igrid=1,ncache,nvector
        ngrid=MIN(nvector,ncache-igrid+1)
        if(ibound==myid)then
           do i=1,ngrid
              ind_grid(i)=active(ilevel)%igrid(igrid+i-1)
           end do
        else if(ibound<=ncpu)then
           do i=1,ngrid
              ind_grid(i)=reception(ibound,ilevel)%igrid(igrid+i-1)
           end do
        else
           do i=1,ngrid
              ind_grid(i)=boundary(ibound-ncpu,ilevel)%igrid(igrid+i-1)
           end do
        end if
     
        ! Loop over cells
        do ind=1,twotondim

           ! Gather cell indices
           iskip=ncoarse+(ind-1)*ngridmax
           do i=1,ngrid
              ind_cell(i)=iskip+ind_grid(i)
           end do
           ! Gather cell centre positions
           do idim=1,ndim
              do i=1,ngrid
                 xx(i,idim)=xg(ind_grid(i),idim)+xc(ind,idim)
              end do
           end do
           ! Rescale position from code units to user units
           do idim=1,ndim
              do i=1,ngrid
                 xx(i,idim)=(xx(i,idim)-skip_loc(idim))/scale
              end do
           end do
           
           ! Call initial condition routine
           ! call condinit(xx,uu,dx_loc,ngrid)

           ! Actually, call boundana for this since we want to reset to
           ! the halo conditions and not the initial conditions at this
           ! particular location
           call boundana(xx,uu,dx_loc,1,ngrid)

           ! Enforce BCs in Halo (Density < Outer Disk Density Contour)
           ! Enforce BCs in Central Region (Spherical Radius < Some Cut-Off)
           ! Reset to ICs
           do i=1,ngrid
              ! Only do any of this if we are in a leaf cell, i.e. have no son cell(s)
              ! The results will be communicated upwards through the AMR hierarchy
              if (son(ind_cell(i)) .EQ. 0) then
                 x0  = xx(i,:) - 0.5 * boxlen
                 xx0 = 0.0d0; yy0 = 0.0d0; zz0 = 0.0d0
                 xx0 = xx(i,1) - 0.5 * boxlen
#if NDIM>1
                 yy0 = xx(i,2) - 0.5 * boxlen
#endif
#if NDIM>2
                 zz0 = xx(i,3) - 0.5 * boxlen
#endif
                 dd0 = sqrt(xx0**2. + yy0**2. + zz0**2.)
                 if (((uold(ind_cell(i),1) .LE. (rho0 * rho0_frac_reset)) .AND. reset_halo) .OR. (dd0 .LE. r_accrete)) then

                    ! Only Count Mass Not in Domain Boundary
                    if ( ibound .EQ. myid ) then

                       ! Mass Difference
                       delta_rho = uold(ind_cell(i),1) - uu(i,1)

                       ! Angular Momentum Difference
                       call compute_angular_momentum(x0, uold(ind_cell(i),:), angular_momentum_old, dx_loc)
                       call compute_angular_momentum(x0, uu(i,:), angular_momentum_new, dx_loc)
                       delta_angular_momentum = angular_momentum_old - angular_momentum_new

                       ! Mass Lost to Accretion (On Star)
                       ! Angular Momentum Lost to Accretion (On Star)
                       if ( dd0 .LE. r_accrete ) then
                          mass_lost_to_star_dt_leaf_loc = mass_lost_to_star_dt_leaf_loc + delta_rho * dx_loc**ndim
                          angular_momentum_lost_to_star_dt_leaf_loc = angular_momentum_lost_to_star_dt_leaf_loc + delta_angular_momentum
                       end if

                       ! Mass Lost to Halo, Not Accreted
                       ! Angular Momentum Lost to Halo
                       if (((uold(ind_cell(i),1) .LE. (rho0 * rho0_frac_reset)) .AND. reset_halo) .AND. (.NOT. (dd0 .LE. r_accrete))) then
                          mass_lost_to_halo_dt_leaf_loc = mass_lost_to_halo_dt_leaf_loc + delta_rho * dx_loc**ndim
                          angular_momentum_lost_to_halo_dt_leaf_loc = angular_momentum_lost_to_halo_dt_leaf_loc + delta_angular_momentum
                       end if

                    end if

                    uold(ind_cell(i),1)=uu(i,1)
                    uold(ind_cell(i),2)=uu(i,2)
                    if(ndim>1)uold(ind_cell(i),3)=uu(i,3)
                    if(ndim>2)uold(ind_cell(i),4)=uu(i,4)
                    uold(ind_cell(i),ndim+2)=uu(i,ndim+2)

                 end if
              end if
           end do

        end do
        ! End loop over cells

     end do
     ! End loop over grids

  end do
  ! End loop over linked lists

111 format('   Entering impose_bc for level ',I2)

end subroutine impose_bc
!#########################################################
!#########################################################
!#########################################################
!#########################################################
subroutine grow_disk(ilevel, Mdot)
  use amr_commons                         ! dtold(ilevel)
  use hydro_commons
  use disk_parameters, only: rho0, rho0_frac_reset
  implicit none
  integer::ilevel

  integer::igrid,ngrid,ncache,i,ind,iskip,ix,iy,iz
  integer::info,ibound,nx_loc,idim
  real(dp)::dx,dx_loc,scale,d,u,v,w,e
  real(kind=8)::rho_max_loc,rho_max_all,epot_loc,epot_all
  real(dp),dimension(1:8,1:3)::xc
  real(dp),dimension(1:3)::skip_loc

  integer ,dimension(1:nvector),save::ind_grid,ind_cell
  real(dp),dimension(1:nvector,1:ndim),save::xx
  real(dp),dimension(1:nvector,1:nvar),save::uu

  real(dp)::xx0,yy0,zz0,dd0

  real(dp)::delta_rho

  real(dp)::Mscale
  real(dp), intent(in)::Mdot

  if(verbose)write(*,111)ilevel

  ! Mesh size at level ilevel in coarse cell units
  dx=0.5D0**ilevel

  ! Rescaling factors
  nx_loc=(icoarse_max-icoarse_min+1)
  skip_loc=(/0.0d0,0.0d0,0.0d0/)
  if(ndim>0)skip_loc(1)=dble(icoarse_min)
  if(ndim>1)skip_loc(2)=dble(jcoarse_min)
  if(ndim>2)skip_loc(3)=dble(kcoarse_min)
  scale=dble(nx_loc)/boxlen
  dx_loc=dx/scale

  ! Set position of cell centers relative to grid center
  do ind=1,twotondim
     iz=(ind-1)/4
     iy=(ind-1-4*iz)/2
     ix=(ind-1-2*iy-4*iz)
     if(ndim>0)xc(ind,1)=(dble(ix)-0.5D0)*dx
     if(ndim>1)xc(ind,2)=(dble(iy)-0.5D0)*dx
     if(ndim>2)xc(ind,3)=(dble(iz)-0.5D0)*dx
  end do

  ! At t=0, we have no old disk mass, so we cannot compute how much mass we need to add
  if(t.EQ.0)then
     Mscale=1.0
  else
     Mscale = 1 + ( Mdot * dtold(ilevel) / mass_disk_tot_last )
  end if

  ! Loop over all AMR linked lists (including physical boundaries).
  do ibound=1,nboundary+ncpu

     if(ibound==myid)then
        ncache=active(ilevel)%ngrid
     else if(ibound<=ncpu)then
        ncache=reception(ibound,ilevel)%ngrid
     else
        ncache=boundary(ibound-ncpu,ilevel)%ngrid
     end if

     ! Loop over grids by vector sweeps
     do igrid=1,ncache,nvector
        ngrid=MIN(nvector,ncache-igrid+1)
        if(ibound==myid)then
           do i=1,ngrid
              ind_grid(i)=active(ilevel)%igrid(igrid+i-1)
           end do
        else if(ibound<=ncpu)then
           do i=1,ngrid
              ind_grid(i)=reception(ibound,ilevel)%igrid(igrid+i-1)
           end do
        else
           do i=1,ngrid
              ind_grid(i)=boundary(ibound-ncpu,ilevel)%igrid(igrid+i-1)
           end do
        end if

        ! Loop over cells
        do ind=1,twotondim

           ! Gather cell indices
           iskip=ncoarse+(ind-1)*ngridmax
           do i=1,ngrid
              ind_cell(i)=iskip+ind_grid(i)
           end do
           ! Gather cell centre positions
           do idim=1,ndim
              do i=1,ngrid
                 xx(i,idim)=xg(ind_grid(i),idim)+xc(ind,idim)
              end do
           end do
           ! Rescale position from code units to user units
           do idim=1,ndim
              do i=1,ngrid
                 xx(i,idim)=(xx(i,idim)-skip_loc(idim))/scale
              end do
           end do

           ! Add Mass in Leaf Cells
           ! @todo - Should we check whether we're in the domain boundary?
           !
           ! To be sure to avoid confusion, u is the vector of CONSERVATIVE
           ! variables, i.e.
           ! u(1) = rho (density)
           ! u(2) = v_x * rho (x-momentum)
           ! u(3) = v_y * rho (y-momentum)
           ! u(4) = v_z * rho (z-momentum)
           ! u(5) = E = P + 0.5 * rho * ( v_tot**2 ) (internal energy)
           !
           ! Therefore, all variables contain the density term.
           ! Therefore, all variables need to be scaled up.
           do i=1,ngrid
              if (son(ind_cell(i)) .EQ. 0) then
                 if ( uold(ind_cell(i),1) .GT. (rho0 * rho0_frac_reset) ) then
                    uold(ind_cell(i),1)=uold(ind_cell(i),1)*Mscale
                    uold(ind_cell(i),2)=uold(ind_cell(i),2)*Mscale
                    if(ndim>1)uold(ind_cell(i),3)=uold(ind_cell(i),3)*Mscale
                    if(ndim>2)uold(ind_cell(i),4)=uold(ind_cell(i),4)*Mscale
                    uold(ind_cell(i),ndim+2)=uold(ind_cell(i),ndim+2)*Mscale
                 end if
              end if
           end do

        end do
        ! End loop over cells

     end do
     ! End loop over grids

  end do
  ! End loop over linked lists

111 format('   Entering impose_bc for level ',I2)

end subroutine grow_disk
