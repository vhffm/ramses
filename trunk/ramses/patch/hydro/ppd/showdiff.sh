#/bin/bash

diff -u ../../../amr/adaptive_loop.f90 adaptive_loop.f90 | less
diff -u ../../../amr/amr_step.f90 amr_step.f90 | less
diff -u ../../../hydro/boundana.f90 boundana.f90 | less
diff -u ../../../hydro/condinit.f90 condinit.f90 | less
diff -u ../../../hydro/cooling_fine.f90 cooling_fine.f90 | less
diff -u ../../../hydro/courant_fine.f90 courant_fine.f90 | less
diff -u ../../../poisson/gravana.f90 gravana.f90 | less
diff -u ../../../hydro/hydro_boundary.f90 hydro_boundary.f90 | less
diff -u ../../../hydro/hydro_commons.f90 hydro_commons.f90 | less
diff -u ../../../hydro/init_flow_fine.f90 init_flow_fine.f90 | less
diff -u ../../../hydro/init_hydro.f90 init_hydro.f90 | less
diff -u ../../../amr/output_amr.f90 output_amr.f90 | less
diff -u ../../../amr/read_params.f90 read_params.f90 | less
diff -u ../../../poisson/rho_ana.f90 rho_ana.f90 | less
diff -u ../../../amr/units.f90 units.f90 | less
diff -u ../../../amr/update_time.f90 update_time.f90 | less
